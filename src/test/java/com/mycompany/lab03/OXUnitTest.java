/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/UnitTests/JUnit5TestClass.java to edit this template
 */
package com.mycompany.lab03;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

/**
 *
 * @author zacop
 */
public class OXUnitTest {

    public OXUnitTest() {
    }

    @BeforeAll
    public static void setUpClass() {
    }

    @AfterAll
    public static void tearDownClass() {
    }

    @BeforeEach
    public void setUp() {
    }

    @AfterEach
    public void tearDown() {
    }

    // TODO add test methods here.
    // The methods must be annotated with annotation . For example:
    //
    // 
    // public void hello() {}
    @Test
    public void testCheckWinRow1_0_O_output_true() {
        String table[][] = {{"O", "O", "O"}, {"-", "-", "-"}, {"-", "-", "-"}};
        String currentPlayer = "O";
        boolean result = XO.checkWin(table, currentPlayer);
        assertEquals(true, result);
    }

    @Test
    public void testCheckWinRow2_0_O_output_true() {
        String table[][] = {{"-", "-", "-"}, {"O", "O", "O"}, {"-", "-", "-"}};
        String currentPlayer = "O";
        boolean result = XO.checkWin(table, currentPlayer);
        assertEquals(true, result);
    }

    @Test
    public void testCheckWinRow3_0_O_output_true() {
        String table[][] = {{"-", "-", "-"}, {"-", "-", "-"}, {"O", "O", "O"}};
        String currentPlayer = "O";
        boolean result = XO.checkWin(table, currentPlayer);
        assertEquals(true, result);
    }

    @Test
    public void testCheckWinRow1_0_O_output_false_1() {
        String table[][] = {{"-", "O", "O"}, {"-", "-", "-"}, {"-", "-", "-"}};
        String currentPlayer = "O";
        boolean result = XO.checkWin(table, currentPlayer);
        assertEquals(false, result);
    }

    @Test
    public void testCheckWinRow1_0_O_output_false_2() {
        String table[][] = {{"O", "-", "O"}, {"-", "-", "-"}, {"-", "-", "-"}};
        String currentPlayer = "O";
        boolean result = XO.checkWin(table, currentPlayer);
        assertEquals(false, result);
    }

    @Test
    public void testCheckWinRow1_0_O_output_false_3() {
        String table[][] = {{"O", "O", "-"}, {"-", "-", "-"}, {"-", "-", "-"}};
        String currentPlayer = "O";
        boolean result = XO.checkWin(table, currentPlayer);
        assertEquals(false, result);
    }

    @Test
    public void testCheckWinRow2_0_O_output_false_1() {
        String table[][] = {{"-", "-", "-"}, {"-", "O", "O"}, {"-", "-", ""}};
        String currentPlayer = "O";
        boolean result = XO.checkWin(table, currentPlayer);
        assertEquals(false, result);
    }

    @Test
    public void testCheckWinRow2_0_O_output_false_2() {
        String table[][] = {{"-", "-", "-"}, {"O", "-", "O"}, {"-", "-", ""}};
        String currentPlayer = "O";
        boolean result = XO.checkWin(table, currentPlayer);
        assertEquals(false, result);
    }

    @Test
    public void testCheckWinRow2_0_O_output_false_3() {
        String table[][] = {{"-", "-", "-"}, {"O", "O", "-"}, {"-", "-", ""}};
        String currentPlayer = "O";
        boolean result = XO.checkWin(table, currentPlayer);
        assertEquals(false, result);
    }

    @Test
    public void testCheckWinRow3_0_O_output_false_1() {
        String table[][] = {{"-", "-", "-"}, {"-", "-", "-"}, {"-", "O", "O"}};
        String currentPlayer = "O";
        boolean result = XO.checkWin(table, currentPlayer);
        assertEquals(false, result);
    }

    @Test
    public void testCheckWinRow3_0_O_output_false_2() {
        String table[][] = {{"-", "-", "-"}, {"-", "-", "-"}, {"O", "-", "O"}};
        String currentPlayer = "O";
        boolean result = XO.checkWin(table, currentPlayer);
        assertEquals(false, result);
    }

    @Test
    public void testCheckWinRow3_0_O_output_false_3() {
        String table[][] = {{"-", "-", "-"}, {"-", "-", "-"}, {"O", "O", "-"}};
        String currentPlayer = "O";
        boolean result = XO.checkWin(table, currentPlayer);
        assertEquals(false, result);
    }

    @Test
    public void testCheckWinCol1_0_O_output_true() {
        String table[][] = {{"O", "-", "-"}, {"O", "-", "-"}, {"O", "-", "-"}};
        String currentPlayer = "O";
        boolean result = XO.checkWin(table, currentPlayer);
        assertEquals(true, result);
    }

    @Test
    public void testCheckWinCol2_0_O_output_true() {
        String table[][] = {{"-", "O", "-"}, {"-", "O", "-"}, {"-", "O", "-"}};
        String currentPlayer = "O";
        boolean result = XO.checkWin(table, currentPlayer);
        assertEquals(true, result);
    }

    @Test
    public void testCheckWinCol3_0_O_output_true() {
        String table[][] = {{"-", "-", "O"}, {"-", "-", "O"}, {"-", "-", "O"}};
        String currentPlayer = "O";
        boolean result = XO.checkWin(table, currentPlayer);
        assertEquals(true, result);
    }

    @Test
    public void testCheckWinCol1_0_O_output_false_1() {
        String table[][] = {{"-", "-", "-"}, {"O", "-", "-"}, {"O", "-", "-"}};
        String currentPlayer = "O";
        boolean result = XO.checkWin(table, currentPlayer);
        assertEquals(false, result);
    }

    @Test
    public void testCheckWinCol1_0_O_output_false_2() {
        String table[][] = {{"O", "-", "-"}, {"-", "-", "-"}, {"O", "-", "-"}};
        String currentPlayer = "O";
        boolean result = XO.checkWin(table, currentPlayer);
        assertEquals(false, result);
    }

    @Test
    public void testCheckWinCol1_0_O_output_false_3() {
        String table[][] = {{"O", "-", "-"}, {"O", "-", "-"}, {"-", "-", "-"}};
        String currentPlayer = "O";
        boolean result = XO.checkWin(table, currentPlayer);
        assertEquals(false, result);
    }

    @Test
    public void testCheckWinCol2_0_O_output_false_1() {
        String table[][] = {{"-", "-", "-"}, {"-", "O", "-"}, {"-", "O", "-"}};
        String currentPlayer = "O";
        boolean result = XO.checkWin(table, currentPlayer);
        assertEquals(false, result);
    }

    @Test
    public void testCheckWinCol2_0_O_output_false_2() {
        String table[][] = {{"-", "O", "-"}, {"-", "-", "-"}, {"-", "O", "-"}};
        String currentPlayer = "O";
        boolean result = XO.checkWin(table, currentPlayer);
        assertEquals(false, result);
    }

    @Test
    public void testCheckWinCol2_0_O_output_false_3() {
        String table[][] = {{"-", "O", "-"}, {"-", "O", "-"}, {"-", "-", "-"}};
        String currentPlayer = "O";
        boolean result = XO.checkWin(table, currentPlayer);
        assertEquals(false, result);
    }
    
    @Test
    public void testCheckWinCol3_0_O_output_false_1() {
        String table[][] = {{"-", "-", "-"}, {"-", "-", "O"}, {"-", "-", "O"}};
        String currentPlayer = "O";
        boolean result = XO.checkWin(table, currentPlayer);
        assertEquals(false, result);
    }
    
    @Test
    public void testCheckWinCol3_0_O_output_false_2() {
        String table[][] = {{"-", "-", "O"}, {"-", "-", "-"}, {"-", "-", "O"}};
        String currentPlayer = "O";
        boolean result = XO.checkWin(table, currentPlayer);
        assertEquals(false, result);
    }
    
    @Test
    public void testCheckWinCol3_0_O_output_false_3() {
        String table[][] = {{"-", "-", "O"}, {"-", "-", "O"}, {"-", "-", "-"}};
        String currentPlayer = "O";
        boolean result = XO.checkWin(table, currentPlayer);
        assertEquals(false, result);
    }
    
    @Test
    public void testCheckWinRow1_0_X_output_true() {
        String table[][] = {{"X", "X", "X"}, {"-", "-", "-"}, {"-", "-", "-"}};
        String currentPlayer = "X";
        boolean result = XO.checkWin(table, currentPlayer);
        assertEquals(true, result);
    }
    
    @Test
    public void testCheckWinRow2_0_X_output_true() {
        String table[][] = {{"-", "-", "-"}, {"X", "X", "X"}, {"-", "-", "-"}};
        String currentPlayer = "X";
        boolean result = XO.checkWin(table, currentPlayer);
        assertEquals(true, result);
    }
    
    @Test
    public void testCheckWinRow3_0_X_output_true() {
        String table[][] = {{"-", "-", "-"}, {"-", "-", "-"}, {"X", "X", "X"}};
        String currentPlayer = "X";
        boolean result = XO.checkWin(table, currentPlayer);
        assertEquals(true, result);
    }
    
    @Test
    public void testCheckWinRow1_0_X_output_false_1() {
        String table[][] = {{"-", "X", "X"}, {"-", "-", "-"}, {"-", "-", "-"}};
        String currentPlayer = "X";
        boolean result = XO.checkWin(table, currentPlayer);
        assertEquals(false, result);
    }
    
    @Test
    public void testCheckWinRow1_0_X_output_false_2() {
        String table[][] = {{"X", "-", "X"}, {"-", "-", "-"}, {"-", "-", "-"}};
        String currentPlayer = "X";
        boolean result = XO.checkWin(table, currentPlayer);
        assertEquals(false, result);
    }
    
    @Test
    public void testCheckWinRow1_0_X_output_false_3() {
        String table[][] = {{"X", "X", "-"}, {"-", "-", "-"}, {"-", "-", "-"}};
        String currentPlayer = "X";
        boolean result = XO.checkWin(table, currentPlayer);
        assertEquals(false, result);
    }
    
    @Test
    public void testCheckWinRow2_0_X_output_false_1() {
        String table[][] = {{"-", "-", "-"}, {"-", "X", "X"}, {"-", "-", ""}};
        String currentPlayer = "X";
        boolean result = XO.checkWin(table, currentPlayer);
        assertEquals(false, result);
    }
    
    @Test
    public void testCheckWinRow2_0_X_output_false_2() {
        String table[][] = {{"-", "-", "-"}, {"X", "-", "X"}, {"-", "-", ""}};
        String currentPlayer = "X";
        boolean result = XO.checkWin(table, currentPlayer);
        assertEquals(false, result);
    }
    
    @Test
    public void testCheckWinRow2_0_X_output_false_3() {
        String table[][] = {{"-", "-", "-"}, {"X", "X", "-"}, {"-", "-", ""}};
        String currentPlayer = "X";
        boolean result = XO.checkWin(table, currentPlayer);
        assertEquals(false, result);
    }
    
    @Test
    public void testCheckWinRow3_0_X_output_false_1() {
        String table[][] = {{"-", "-", "-"}, {"-", "-", "-"}, {"-", "X", "X"}};
        String currentPlayer = "X";
        boolean result = XO.checkWin(table, currentPlayer);
        assertEquals(false, result);
    }
    
    @Test
    public void testCheckWinRow3_0_X_output_false_2() {
        String table[][] = {{"-", "-", "-"}, {"-", "-", "-"}, {"X", "-", "X"}};
        String currentPlayer = "X";
        boolean result = XO.checkWin(table, currentPlayer);
        assertEquals(false, result);
    }
    
    @Test
    public void testCheckWinRow3_0_X_output_false_3() {
        String table[][] = {{"-", "-", "-"}, {"-", "-", "-"}, {"X", "X", "-"}};
        String currentPlayer = "X";
        boolean result = XO.checkWin(table, currentPlayer);
        assertEquals(false, result);
    }
    
    @Test
    public void testCheckWinCol1_0_X_output_true() {
        String table[][] = {{"X", "-", "-"}, {"X", "-", "-"}, {"X", "-", "-"}};
        String currentPlayer = "X";
        boolean result = XO.checkWin(table, currentPlayer);
        assertEquals(true, result);
    }

    @Test
    public void testCheckWinCol2_0_X_output_true() {
        String table[][] = {{"-", "X", "-"}, {"-", "X", "-"}, {"-", "X", "-"}};
        String currentPlayer = "X";
        boolean result = XO.checkWin(table, currentPlayer);
        assertEquals(true, result);
    }
    
    @Test
    public void testCheckWinCol3_0_X_output_true() {
        String table[][] = {{"-", "-", "X"}, {"-", "-", "X"}, {"-", "-", "X"}};
        String currentPlayer = "X";
        boolean result = XO.checkWin(table, currentPlayer);
        assertEquals(true, result);
    }
    
    @Test
    public void testCheckWinCol1_0_X_output_false_1() {
        String table[][] = {{"-", "-", "-"}, {"X", "-", "-"}, {"X", "-", "-"}};
        String currentPlayer = "X";
        boolean result = XO.checkWin(table, currentPlayer);
        assertEquals(false, result);
    }
    
    @Test
    public void testCheckWinCol1_0_X_output_false_2() {
        String table[][] = {{"X", "-", "-"}, {"-", "-", "-"}, {"X", "-", "-"}};
        String currentPlayer = "X";
        boolean result = XO.checkWin(table, currentPlayer);
        assertEquals(false, result);
    }
    
    @Test
    public void testCheckWinCol1_0_X_output_false_3() {
        String table[][] = {{"X", "-", "-"}, {"X", "-", "-"}, {"-", "-", "-"}};
        String currentPlayer = "X";
        boolean result = XO.checkWin(table, currentPlayer);
        assertEquals(false, result);
    }
    
    
    @Test
    public void testCheckWinCol2_0_X_output_false_1() {
        String table[][] = {{"-", "-", "-"}, {"-", "X", "-"}, {"-", "X", "-"}};
        String currentPlayer = "X";
        boolean result = XO.checkWin(table, currentPlayer);
        assertEquals(false, result);
    }
    
    @Test
    public void testCheckWinCol2_0_X_output_false_2() {
        String table[][] = {{"-", "X", "-"}, {"-", "-", "-"}, {"-", "X", "-"}};
        String currentPlayer = "X";
        boolean result = XO.checkWin(table, currentPlayer);
        assertEquals(false, result);
    }
    
    @Test
    public void testCheckWinCol2_0_X_output_false_3() {
        String table[][] = {{"-", "X", "-"}, {"-", "X", "-"}, {"-", "-", "-"}};
        String currentPlayer = "X";
        boolean result = XO.checkWin(table, currentPlayer);
        assertEquals(false, result);
    }
    
    @Test
    public void testCheckWinCol3_0_X_output_false_1() {
        String table[][] = {{"-", "-", "-"}, {"-", "-", "X"}, {"-", "-", "X"}};
        String currentPlayer = "X";
        boolean result = XO.checkWin(table, currentPlayer);
        assertEquals(false, result);
    }
    
    @Test
    public void testCheckWinCol3_0_X_output_false_2() {
        String table[][] = {{"-", "-", "X"}, {"-", "-", "-"}, {"-", "-", "X"}};
        String currentPlayer = "X";
        boolean result = XO.checkWin(table, currentPlayer);
        assertEquals(false, result);
    }
    
    @Test
    public void testCheckWinCol3_0_X_output_false_3() {
        String table[][] = {{"-", "-", "X"}, {"-", "-", "X"}, {"-", "-", "-"}};
        String currentPlayer = "X";
        boolean result = XO.checkWin(table, currentPlayer);
        assertEquals(false, result);
    }
    
    @Test
    public void testCheckWinDiagonal_Left_O_output_True() {
        String table[][] = {{"O", "-", "-"}, {"-", "O", "-"}, {"-", "-", "O"}};
        String currentPlayer = "O";
        boolean result = XO.checkWin(table, currentPlayer);
        assertEquals(true, result);
    }
    
    @Test
    public void testCheckWinDiagonal_Left_X_output_True() {
        String table[][] = {{"X", "-", "-"}, {"-", "X", "-"}, {"-", "-", "X"}};
        String currentPlayer = "X";
        boolean result = XO.checkWin(table, currentPlayer);
        assertEquals(true, result);
    }
    
    @Test
    public void testCheckWinDiagonal_Right_O_output_True() {
        String table[][] = {{"-", "-", "O"}, {"-", "O", "-"}, {"O", "-", "-"}};
        String currentPlayer = "O";
        boolean result = XO.checkWin(table, currentPlayer);
        assertEquals(true, result);
    }
    
    @Test
    public void testCheckWinDiagonal_Right_X_output_True() {
        String table[][] = {{"-", "-", "X"}, {"-", "X", "-"}, {"X", "-", "-"}};
        String currentPlayer = "X";
        boolean result = XO.checkWin(table, currentPlayer);
        assertEquals(true, result);
    }
    
    @Test
    public void testCheckWinDiagonal_Left_O_output_False_0_0() {
        String table[][] = {{"-", "-", "-"}, {"-", "O", "-"}, {"-", "-", "O"}};
        String currentPlayer = "O";
        boolean result = XO.checkWin(table, currentPlayer);
        assertEquals(false, result);
    }
    
    @Test
    public void testCheckWinDiagonal_Left_O_output_False_1_1() {
        String table[][] = {{"O", "-", "-"}, {"-", "-", "-"}, {"-", "-", "O"}};
        String currentPlayer = "O";
        boolean result = XO.checkWin(table, currentPlayer);
        assertEquals(false, result);
    }
    
    @Test
    public void testCheckWinDiagonal_Left_O_output_False_2_2() {
        String table[][] = {{"O", "-", "-"}, {"-", "O", "-"}, {"-", "-", "-"}};
        String currentPlayer = "O";
        boolean result = XO.checkWin(table, currentPlayer);
        assertEquals(false, result);
    }
    
    @Test
    public void testCheckWinDiagonal_Left_X_output_False_0_0() {
        String table[][] = {{"-", "-", "-"}, {"-", "X", "-"}, {"-", "-", "X"}};
        String currentPlayer = "X";
        boolean result = XO.checkWin(table, currentPlayer);
        assertEquals(false, result);
    }
    
    @Test
    public void testCheckWinDiagonal_Left_X_output_False_1_1() {
        String table[][] = {{"X", "-", "-"}, {"-", "-", "-"}, {"-", "-", "X"}};
        String currentPlayer = "X";
        boolean result = XO.checkWin(table, currentPlayer);
        assertEquals(false, result);
    }
    
    @Test
    public void testCheckWinDiagonal_Left_X_output_False_2_2() {
        String table[][] = {{"X", "-", "-"}, {"-", "X", "-"}, {"-", "-", "-"}};
        String currentPlayer = "X";
        boolean result = XO.checkWin(table, currentPlayer);
        assertEquals(false, result);
    }
    
    @Test
    public void testCheckWinDiagonal_Right_O_output_False_2_0() {
        String table[][] = {{"-", "-", "O"}, {"-", "O", "-"}, {"-", "-", "-"}};
        String currentPlayer = "O";
        boolean result = XO.checkWin(table, currentPlayer);
        assertEquals(false, result);
    }
    
    @Test
    public void testCheckWinDiagonal_Right_O_output_False_1_1() {
        String table[][] = {{"-", "-", "O"}, {"-", "-", "-"}, {"O", "-", "-"}};
        String currentPlayer = "O";
        boolean result = XO.checkWin(table, currentPlayer);
        assertEquals(false, result);
    }
    
    @Test
    public void testCheckWinDiagonal_Right_O_output_False_0_2() {
        String table[][] = {{"-", "-", "-"}, {"-", "O", "-"}, {"O", "-", "-"}};
        String currentPlayer = "O";
        boolean result = XO.checkWin(table, currentPlayer);
        assertEquals(false, result);
    }
    
    @Test
    public void testCheckWinDiagonal_Right_X_output_False_0_2() {
        String table[][] = {{"-", "-", "-"}, {"-", "X", "-"}, {"X", "-", "-"}};
        String currentPlayer = "X";
        boolean result = XO.checkWin(table, currentPlayer);
        assertEquals(false, result);
    }
    
    @Test
    public void testCheckWinDiagonal_Right_X_output_False_1_1() {
        String table[][] = {{"-", "-", "X"}, {"-", "-", "-"}, {"X", "-", "-"}};
        String currentPlayer = "X";
        boolean result = XO.checkWin(table, currentPlayer);
        assertEquals(false, result);
    }
    
    
}
