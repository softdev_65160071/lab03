/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 */
package com.mycompany.lab03;

import java.util.Scanner;

/**
 *
 * @author zacop
 */
public class XO {

    static String currentPlayer = "X";
    static String table[][] = {{"-","-","-"}, {"-","-","-"}, {"-", "-", "-"}};

    private static boolean checkRow(String table[][],String currentPlayer){
        for(int row=0; row < 3; row++){
            if(checkRow(table,currentPlayer,row)){
                return true;
            }
        }
        return false;
    }
    
    private static boolean checkRow(String table[][], String currentPlayer, int row) {
        if(table[row][0].equals(currentPlayer) && table[row][1].equals(currentPlayer) && table[row][2].equals(currentPlayer)){
            return true;
        }return false;
    }
    
    private static boolean checkCol(String table[][],String currentPlayer){
        for(int col=0; col < 3; col++){
            if(checkCol(table,currentPlayer,col)){
                return true;
            }
        }
        return false;
    }
    
    private static boolean checkCol(String table[][], String currentPlayer, int col) {
        if(table[0][col].equals(currentPlayer) && table[1][col].equals(currentPlayer) && table[2][col].equals(currentPlayer)){
            return true;
        }return false;
    }
    
    private static boolean checkDiagonal_Left(String table[][],String currentPlayer){
        if(table[0][0].equals(currentPlayer) && table[1][1].equals(currentPlayer) && table[2][2].equals(currentPlayer)){
            return true;
        }return false;
    }
    
    private static boolean checkDiagonal_Right(String table[][],String currentPlayer){
        if(table[2][0].equals(currentPlayer) && table[1][1].equals(currentPlayer) && table[0][2].equals(currentPlayer)){
            return true;
        }return false;
    }
    
    static boolean checkWin(String table[][],String currentPlayer){
        if (checkRow(table,currentPlayer)|| checkCol(table,currentPlayer)||checkDiagonal_Left(table,currentPlayer)||checkDiagonal_Right(table,currentPlayer)){
            return true;
        }return false;
    }
}